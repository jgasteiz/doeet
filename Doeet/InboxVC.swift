//
//  InboxVC.swift
//  Doeet
//
//  Created by Javi Manzano on 30/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Firebase
import UIKit

class InboxVC: UIViewController {
    
    let todoCellIdentifier = "TodoCell"
    
    // List of the todos displayed in the table view.
    var todoList: [Todo] = []
    
    // Authenticated user id.
    var user: FIRUser?
    
    // Controller for dealing with todos.
    var todosController: TodosController?
    
    @IBOutlet weak var newTodoTextView: UITextField!
    
//    @IBOutlet weak var userPhotoView: UIView!
//    @IBOutlet weak var userPhoto: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        todosController = TodosController(user: user)
        
        // Auto height for the table rows.
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
        automaticallyAdjustsScrollViewInsets = true
        
        todosController?.initializeTodosResource(
            onTodoAdd: onTodoAdd,
            onTodoDelete: onTodoDelete
        )
    }
    
    override func viewDidAppear(animated: Bool) {
        
//        userPhotoView.layer.cornerRadius = 18
//        userPhoto.layer.cornerRadius = 18
//        
//        // Set the user photo in the nav bar button
//        if let user = user, let photoUrl = user.photoURL {
//            userPhoto.image = UIImage(data: NSData(contentsOfURL: photoUrl)!)
//        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowSettings" {
            let destinationVC = segue.destinationViewController as! SettingsVC
            destinationVC.user = user
        }
    }

    @IBAction func doneEditing(sender: AnyObject) {
        if let todoText = newTodoTextView.text, let user = user {
            todosController?.addTodo(Todo(text: todoText), uid: user.uid)
            newTodoTextView.text = ""
        }
    }
    
    @IBAction func userSettings(sender: AnyObject) {
        performSegueWithIdentifier("ShowSettings", sender: self)
    }
    
    @IBAction func unwindToInbox(segue: UIStoryboardSegue) {}
    
    
    private func onTodoAdd (todo: Todo) {
        todoList.insert(todo, atIndex: 0)
        tableView.insertRowsAtIndexPaths(
            [NSIndexPath(forRow: 0, inSection: 0)],
            withRowAnimation: .Automatic)
    }
    
    private func onTodoDelete (todo: Todo) {
        if let todoIndex = todoList.indexOf({ $0.id == todo.id }) {
            todoList.removeAtIndex(todoIndex)
            tableView.deleteRowsAtIndexPaths(
                [NSIndexPath(forRow: todoIndex, inSection: 0)],
                withRowAnimation: .Automatic)
        }
    }
}

extension InboxVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(todoCellIdentifier) as! TodoCell
        cell.setContent(todo: todoList[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete, let user = user {
            todosController?.deleteTodo(todoList[indexPath.row], uid: user.uid, rowIndex: indexPath.row)
        }
    }
}

