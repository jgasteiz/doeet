//
//  GoogleSignInVC.swift
//  Doeet
//
//  Created by Javi Manzano on 31/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Firebase
import UIKit

class GoogleSignInVC: UIViewController, GIDSignInUIDelegate {
    
    var user: FIRUser? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        FIRAuth.auth()?.addAuthStateDidChangeListener({ (auth, user) in
            if let user = user {
                self.user = user
                self.performSegueWithIdentifier("StartApp", sender: self)
            }
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "StartApp" {
            let nav = segue.destinationViewController as! UINavigationController
            let destinationVC = nav.topViewController as! InboxVC
            
            destinationVC.user = user
        }
    }
    
    @IBAction func unwindToLogin(segue: UIStoryboardSegue) {}
}
