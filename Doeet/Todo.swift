//
//  Todo.swift
//  Doeet
//
//  Created by Javi Manzano on 30/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Foundation

class Todo {
    
    var id: String?
    var text: String?
    
    init () {
        
    }
    
    init (text: String) {
        self.text = text
    }
    
    init (id: String, text: String) {
        self.id = id
        self.text = text
    }
}