//
//  TodosController.swift
//  Doeet
//
//  Created by Javi Manzano on 30/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Firebase
import Foundation

class TodosController {
    
    // Authenticated user id
    private var user: FIRUser?
    
    private var todosRef: FIRDatabaseReference? {
        if let user = user {
            return FIRDatabase.database().reference().child("todos").child(user.uid)
        }
        return nil
    }
    
    init (user: FIRUser?) {
        if let user = user {
            self.user = user
        } else {
            self.user = nil
        }
    }
    
    /*
     Fetch all todos and set up event observers.
    */
    func initializeTodosResource (onTodoAdd blockAdd: (Todo) -> Void, onTodoDelete blockDelete: (Todo) -> Void) {
        
        todosRef?.observeEventType(.ChildAdded, withBlock: { (snapshot) -> Void in
            let todo = Todo(
                id: snapshot.key,
                text: snapshot.value!.objectForKey("text") as! String
            )
            blockAdd(todo)
        })
        
        todosRef?.observeEventType(.ChildRemoved, withBlock: { (snapshot) -> Void in
            let todo = Todo(
                id: snapshot.key,
                text: snapshot.value!.objectForKey("text") as! String
            )
            
            blockDelete(todo)
        })
    }
    
    /*
     Delete a given todo from the storage.
     */
    func addTodo (todo: Todo, uid: String) {
        let todo = [
            NSString(string: "uid"): NSString(string: uid),
            NSString(string: "text"): NSString(string: todo.text!)
        ]
        
        todosRef?.childByAutoId().setValue(todo as AnyObject)
    }
    
    /*
     Delete a given todo from the storage.
    */
    func deleteTodo (todo: Todo, uid: String, rowIndex: Int) {
        todosRef?.child(todo.id!).removeValue()
    }
}