//
//  SettingsVC.swift
//  Doeet
//
//  Created by Javi Manzano on 02/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Firebase
import UIKit

class SettingsVC: UIViewController {
    
    var user: FIRUser?
    
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the user photo in the nav bar button
        if let user = user, let photoUrl = user.photoURL {
            userPhotoImageView.layer.cornerRadius = userPhotoImageView.frame.size.width / 2
            
            userPhotoImageView.image = UIImage(data: NSData(contentsOfURL: photoUrl)!)
            userNameLabel.text = user.displayName
        }
    }

    @IBAction func signOut (sender: AnyObject) {
        try! FIRAuth.auth()!.signOut()
        performSegueWithIdentifier("unwindToLogin", sender: self)
    }

    
    @IBAction func done(sender: AnyObject) {
        performSegueWithIdentifier("unwindToInbox", sender: self)
    }
}
