//
//  TodoCell.swift
//  Doeet
//
//  Created by Javi Manzano on 30/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Foundation
import UIKit

class TodoCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    override var bounds: CGRect {
        didSet {
            contentView.frame = bounds
        }
    }
    
    func setContent (todo todo: Todo) {
        title.text = todo.text
    }
}